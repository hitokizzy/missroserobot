from MadamRose.sample_config import Config


class Development(Config):
    OWNER_ID = 1660908473  # my telegram ID
    OWNER_USERNAME = "DoctorStrangeRobot"  # my telegram username
    API_KEY = "1748407681:AAHDsuCxZLQtyToHHrqKNebgt7PPHzhdkNE"  # my api key, as provided by the botfather
    SQLALCHEMY_DATABASE_URI = 'postgresql://username:password@localhost:5432/database'  # sample db credentials
    MESSAGE_DUMP = '-1001276283101' # some group chat that your bot is a member of
    USE_MESSAGE_DUMP = True
    SUDO_USERS = [1219026298, 1645872315, 1173373896]  # List of id's for users which have sudo access to the bot.
    LOAD = []
    NO_LOAD = ['translation']
